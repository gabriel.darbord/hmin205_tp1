package com.example.hmin205_tp1_exo8;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends Activity {
    private ArrayAdapter<String> schedules_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // récupération de la vue ListView qui sert à afficher les horaires, et instantiation
        // de son adaptateur que l'on conserve dans un attribut car on va vouloir le modifier
        ListView lv = findViewById(R.id.list_schedule);
        schedules_adapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item);
        lv.setAdapter(schedules_adapter);

        // création d'un adaptateur pour les choix possibles
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.cities_array));

        // application de l'adaptateur sur les vues de saisie
        AutoCompleteTextView tv;
        tv = findViewById(R.id.query_departure);
        tv.setAdapter(adapter);
        tv = findViewById(R.id.query_destination);
        tv.setAdapter(adapter);

        /* Test d'affichage dans la ListView
        ArrayList<String> schedules = new ArrayList<>();
        schedules.add("foo");
        schedules.add("bar");
        schedules.add("baz");
        showSchedules(schedules);
         */
    }


    // Méthode appelée pour afficher des horaires de train, contenus dans le paramètre schedules.
    // Comme il est uniquement question de développer l’interface graphique, cette méthode
    // n'est jamais vraiment appelée, mais elle pourrait l'être après saisie et validation
    // des champs départ et arrivée.
    public void showSchedules(ArrayList<String> schedules) {
        schedules_adapter.clear();
        schedules_adapter.addAll(schedules);
        schedules_adapter.notifyDataSetChanged();
    }
}