package com.example.hmin205_tp1_exo5_6_7;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class CallActivity extends Activity {
    private static final int PERMISSION_REQUEST_CALL = 1;
    private String phoneNum;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_call);

        phoneNum = getIntent().getExtras().getString("phoneNum");
        TextView tv = findViewById(R.id.call_telephone);
        tv.setText(phoneNum);
    }


    // vérifie la permission de l'activité pour faire un appel
    public void tryCall(View v) {
        // si l'application n'a pas la permission, on fait la demande
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    CallActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    PERMISSION_REQUEST_CALL);
        } else {
            makeCall();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // si la permission a été donnée, exécution de l'appel téléphonique
        if (requestCode == PERMISSION_REQUEST_CALL
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            makeCall();
        }
    }


    // exécution de l'appel téléphonique, uniquement appelée si l'activité possède la permission
    private void makeCall() {
        Intent call = new Intent(Intent.ACTION_CALL);
        call.setData(Uri.parse("tel:" + phoneNum));
        startActivity(call);
    }
}
