package com.example.hmin205_tp1_exo5_6_7;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ConfirmActivity extends Activity {
    private String phoneNum;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_confirm);

        final int[] entryIds = new int[]{R.id.confirm_nom, R.id.confirm_prenom, R.id.confirm_age, R.id.confirm_competences, R.id.confirm_age, R.id.confirm_telephone};
        final int phoneNumIndex = 5;

        // récupération des valeurs des champs
        String[] entryValues = getIntent().getExtras().getStringArray("entries");

        for (int i=0; i<MainActivity.NB_FIELDS; i++) {
            TextView entry = findViewById(entryIds[i]);
            entry.setText(entryValues[i]);
        }

        phoneNum = entryValues[phoneNumIndex];
    }


    // termine l'activité
    public void goBack(View v) {
        finish();
    }


    // lance une autre activité et termine celle-ci
    public void confirmed(View v) {
        Intent call = new Intent(ConfirmActivity.this, CallActivity.class);
        // transmission du numéro de téléphone
        call.putExtra("phoneNum", phoneNum);
        startActivity(call);
        finish();
    }
}
