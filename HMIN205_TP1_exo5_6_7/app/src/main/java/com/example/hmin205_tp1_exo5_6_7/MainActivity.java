package com.example.hmin205_tp1_exo5_6_7;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
    public static final int NB_FIELDS = 6;
    private static final int[] fieldIds = new int[]{R.id.main_nom, R.id.main_prenom, R.id.main_age, R.id.main_competences, R.id.main_age, R.id.main_telephone};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
    }


    // affiche un dialogue demandant une confirmation, et appelle la méthode validate()
    public void confirmValidate(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setMessage(R.string.confirmation)
               .setPositiveButton(R.string.oui, (dialog, id) -> MainActivity.this.validate())
               .setNegativeButton(R.string.non, null)
               .show();
    }


    // vérifie que tous les champs sont remplis et démarre l'activité ConfirmActivity
    public void validate() {
        final String[] fieldValues = new String[NB_FIELDS];
        boolean valid = true;
        String errorMsg = getResources().getString(R.string.requis);

        // récupération de la valeur de chaque champ et vérification
        for(int i=0; i<NB_FIELDS; i++) {
            EditText field = findViewById(fieldIds[i]);
            fieldValues[i] = field.getText().toString();

            // un champ est valide s'il n'est pas vide
            if (fieldValues[i].isEmpty()) {
                valid = false;
                field.setError(errorMsg);
            }
        }

        // si tous les champs sont valides, l'activité est lancée
        if(valid) {
            Intent confirm = new Intent(MainActivity.this, ConfirmActivity.class);
            // les valeurs des champs sont transmises
            confirm.putExtra("entries", fieldValues);
            startActivity(confirm);
        }
    }
}
